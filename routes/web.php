<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
	
	Route::get('/', 'HomeController@index')->name('dashboard.index');
	Route::resource('/rencana-kegiatan', 'RencanaController');
	Route::get('/realisasi-kegiatan/{id_timesheet}/create', 'RealisasiController@create');
	Route::resource('/realisasi-kegiatan', 'RealisasiController')->except('create');
    Route::post('job-order/nilai', 'JobOrderController@addNilai')->name('job-order.add-nilai');
	Route::get('/job-order/{timesheet_id}/comment', 'JobOrderController@comment')->name('job-order.comment');
	Route::post('/job-order/{timesheet_id}/comment', 'JobOrderController@storeComment')->name('job-order.store-comment');
    Route::post('/job-order/{employee_id}', 'JobOrderController@store')->name('job-order.store');
	Route::resource('/job-order', 'JobOrderController')->except('store');
	Route::get('/report', 'ReportController@index')->name('report.index');
	Route::post('/report', 'ReportController@show')->name('report.show');
	Route::get('/chart', 'ChartController@index')->name('chart.index');
	Route::post('/chart', 'ChartController@show')->name('chart.show');

	Route::group(['middleware' => ['role:Admin']], function () {
        Route::resource('/users', 'UserController');
        Route::resource('/tanggal-libur', 'TanggalLiburController');

	});

});

Route::get('/asd', 'ChartController@asd');

Route::get('/home', function(){
    return redirect('/');
});