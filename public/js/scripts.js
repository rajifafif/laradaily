$(document).ready(function(){
	$('.summernote').summernote();
	
	//datepicker report dan chart
	$('.datepicker').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		locale: {
			format: 'DD-MM-YYYY',},
		minYear: 2019,
		maxYear: 2025,
        
	});
	
	//datepicker rencana
	var today = new Date(); 
	//Limit Input sebelum jam 7
	if (today.getHours() < 7) {
		var dd = today.getDate();
	} else {
		var dd = today.getDate()+1;
	}
    var mm = today.getMonth()+1; //January is 0! 
    var yyyy = today.getFullYear(); 
    if(dd<10){ dd='0'+dd } 
    if(mm<10){ mm='0'+mm } 
	var today = dd+'/'+mm+'/'+yyyy; 
	
	var datePickerRencana = $('.datepickerrencana');
	if (datePickerRencana.hasClass('edited')) {
		datePickerRencana.daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			locale: {
				format: 'DD-MM-YYYY',},
			minYear: 2020,
			maxYear: 2025,
            minDate: new Date() 
		});
	} else {
		datePickerRencana.daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			locale: {
				format: 'DD-MM-YYYY',},
			minYear: 2020,
			maxYear: 2025,
			minDate: new Date() 
			
		});
	}
    
	
	//timepicker 
	$('.timepicker').datetimepicker({
        format: 'HH:mm',
        pickDate: false,
        pickSeconds: false,
        pick12HourFormat: false,
        stepping: 5,
    });
});