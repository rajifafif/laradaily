$(document).ready(function () {
	$('.summernote').summernote();

	$('.datepicker').daterangepicker({
		showButtonpanel: false,
		singleDatePicker: true,
		showDropdowns: true,
		locale: {
			format: 'DD-MM-YYYY'
	    },
		minYear: 2019,
		maxYear: 2025
	});

	$('.timepicker').datetimepicker({
        format: 'HH:mm',
        pickDate: false,
        pickSeconds: false,
        pick12HourFormat: false,
        stepping: 5
    });
});