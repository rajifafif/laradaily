<!-- Notifications Dropdown Menu -->
<li class="nav-item dropdown">
  <a class="nav-link" data-toggle="dropdown" href="#">
    <i class="fas fa-cog"></i>
  </a>
  
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
  <span class="dropdown-header">{{ optional(auth()->user())->name }}</span>
  <div class="dropdown-divider"></div>
  <a href="#" class="dropdown-item" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
    <i class="fas fa-sign-out-alt mr-2 text-danger"></i> Logout
  </a>
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
  </form>
    <!--
  <div class="dropdown-divider"></div>
  <!-- <div class="dropdown-divider"></div>
  <a href="#" class="dropdown-item">
    <i class="fas fa-users mr-2"></i> 8 friend requests
    <span class="float-right text-muted text-sm">12 hours</span>
  </a>
  <div class="dropdown-divider"></div>
  <a href="#" class="dropdown-item">
    <i class="fas fa-file mr-2"></i> 3 new reports
    <span class="float-right text-muted text-sm">2 days</span>
  </a> 
  <a href="#" class="dropdown-item dropdown-footer"></a>
    --->
</div>
</li>