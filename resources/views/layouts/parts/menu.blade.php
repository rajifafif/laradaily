<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
        {{--
    <!--
    <li class="nav-item has-treeview menu-open">
      <a href="#" class="nav-link active">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Starter Pages
          <i class="right fas fa-angle-left"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="#" class="nav-link active">
            <i class="far fa-circle nav-icon"></i>
            <p>Active Page</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Inactive Page</p>
          </a>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon fas fa-th"></i>
        <p>
          Simple Link
          <span class="right badge badge-danger">New</span>
        </p>
      </a>
    </li>
    -->
    --}}
        <li class="nav-item">
            <a href="{{ route('dashboard.index') }}" class="nav-link {{ (request()->is('/')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-th"></i>
                <p>Dashboard</p>
            </a>
        </li>
        @hasanyrole('KaBiro|GM|Manager|Staff')       
        <li class="nav-item">
            <a href="{{ route('rencana-kegiatan.index') }}" class="nav-link {{ (request()->is('rencana-kegiatan*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-calendar-plus"></i>
                <p>Rencana Kegiatan</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('KaBiro|GM|Manager|Staff') 
        <li class="nav-item">
            <a href="{{ route('realisasi-kegiatan.index') }}" class="nav-link {{ (request()->is('realisasi-kegiatan*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-calendar-check"></i>
                <p>Realisasi Kegiatan</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('KaBiro|Direksi|GM|Manager|Admin')
        <li class="nav-item">
            <a href="{{ route('job-order.index') }}" class="nav-link {{ (request()->is('job-order*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-exclamation"></i>
                <p>Job Order Kegiatan Staff</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('KaBiro|Direksi|GM|Manager|Admin')       
        <li class="nav-item">
            <a href="{{ route('report.index') }}" class="nav-link {{ (request()->is('report*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-clipboard"></i>
                <p>Report each division</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('KaBiro|Direksi|GM|Manager|Admin')     
        <li class="nav-item">
            <a href="{{ route('chart.index') }}" class="nav-link {{ (request()->is('chart*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-chart-bar"></i>
                <p>Chart isian Daily Activity</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('Staff|Admin|Manager|KaBiro')      
        <li class="nav-item">
            <a href="{{ route('realisasi-kegiatan.index') }}" class="nav-link {{ (request()->is('realisasi-kegiatan*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-history"></i>
                <p>Agenda Kegiatan</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('Admin')      
        <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link {{ (request()->is('users*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-users"></i>
                <p>Users</p>
            </a>
        </li>
        @endhasanyrole
        @hasanyrole('Admin')      
        <li class="nav-item">
            <a href="{{ route('tanggal-libur.index') }}" class="nav-link {{ (request()->is('tanggal-libur*')) ? 'active' : '' }}">
                <i class="nav-icon fas fa-calendar"></i>
                <p>Input tanggal libur</p>
            </a>
        </li>
        @endhasanyrole
    </ul>
</nav>
<!-- /.sidebar-menu -->