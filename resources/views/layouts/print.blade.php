<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Report | Daily Activity</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <style type="text/css">
    .table {
        border-collapse: collapse;
        border-style: solid;
        border-color: black;
        border-size: 1px;
        width: 100%;
        font-size: 14px;
    }

    .table td, .table th {
        border-style: solid;
        border-color: black;
        border-size: 1px;
    }
  </style>
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    @yield('content')
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
