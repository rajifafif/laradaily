@extends('layouts.main')

@section('title', 'TanggalLibur')

@section('content')
<div class="row">
    <div class="col-lg-12">

        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">List Tanngal Libur</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="p-2">
                    <div class="row">
                        <div class="col-md-4">
                            <form method="GET" id="search">
                                <div class="form-group">
                                    <input name="search" class="form-control" placeholder="Search" value="{{ old('search') }}">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2">
                            <a href="{{ route('tanggal-libur.create') }}" class="btn btn-primary">Tambah Hari Libur</a>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr class="text-center">
                            <!-- <th class="align-middle">No</th> -->
                            <th class="align-middle">#ID</th>
                            <th class="align-middle">Tanggal Libur</th>
                            <th class="align-middle">Keterangan Libur</th>
                            <th class="align-middle">Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach ($TanggalLibur as $tbl_libur)
                        <tr>
                            <td>{{ $tbl_libur->idTgl_libur }}</td>
                            <td>{{ $tbl_libur->tgl_libur }}</td>
                            <td>{{ $tbl_libur->keterangan_libur }}</td>
                            <td>
                               <div class="d-flex">
                                <a class="btn btn-sm btn-primary" href="{{ route('tanggal-libur.edit', $tbl_libur) }}"><i class="fas fa-edit"></i></a>
                                &nbsp;
                                <form id="delete-user-{{$tbl_libur->id }}" method="POST" action="{{ route('tanggal-libur.destroy', $tbl_libur) }}" onsubmit="return confirm('Are You Sure to Delete : {{ $tbl_libur->name }}')">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
        </div>

    </div>
</div>
<!-- /.row -->
@endsection