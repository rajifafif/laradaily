@extends('layouts.main')

@section('title', 'Tanggal Libur')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit User</h3>
            </div>
            <br>
                <form action="{{ route('tbl_libur.update', $tanggal-libur) }}" method="POST">
                    @method('PATCH')
                    @include('tanggal-libur._form')
                </form>
            </div>
        </div>
          
      </div>
    <!-- /.row -->
@endsection