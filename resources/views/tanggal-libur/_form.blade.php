<br>
@csrf

<div class="form-group row">
  <label for="Keterangan_libur" class="col-md-2 col-form-label text-md-right">Nama Hari Libur</label>
  <div class="col-md-6">
    
    <input type="text" class="form-control" rows="5" name="name" id="name" placeholder="Nama Lengkap" value="{{ old('name', $user->name ?? '') }}">
  
  </div>
</div>

<div class="form-group row">
  <label for="tgl_libur" class="col-md-2 col-form-label text-md-right">Nickname</label>
  <div class="col-md-6">
    
    <input type="text" class="form-control" rows="5" name="nickname" id="nickname" placeholder="XXX" value="{{ old('nickname', $user->nickname ?? '') }}">
  
  </div>
</div>

<!-- SELECT UNIT -->
<div class="form-group row">
  <label for="date" class="col-md-2 col-form-label text-md-right">Unit Kerja</label>
  <div class="col-md-6">
    <select name="unit_kerja_id" id="unit_kerja_id" class="form-control" data-selected="{{old('unit_kerja_id', $user->unit_kerja_id ?? '')}}">
      @if(count($divisis) > 1)
        <option value="">Pilih Unit Kerja</option>
      @endif
      @foreach($divisis as $divisi)
      @foreach($divisi->unit_kerjas as $unit_kerja)
        <option class="unit_kerja_option" 
                data-divisi_id="{{ $divisi->idDivisi }}" 
                value="{{ $unit_kerja->idUnitKerja }}" 
                {{ $unit_kerja->idUnitKerja == old('unit_kerja_id', $user->unit_kerja_id ?? '') ? 'selected' : '' }} 
                style="display: none;">
            {{ $unit_kerja->unitKerja }}
        </option>
      @endforeach
      @endforeach
    </select>
  </div>
</div>


<div class="form-group row">
  <label for="active" class="col-md-2 col-form-label text-md-right">Status</label>
  <div class="col-md-6">
    <select name="active" id="active" class="form-control">
        <option value="1" {{ old('active', $user->active ?? 1) == 1 ? 'selected' : '' }}>Aktif</option>
        <option value="0" {{ old('active', $user->active ?? 1) == 0 ? 'selected' : '' }}>Tidak Aktif</option>
    </select>
  </div>
</div>

<div class="form-group row">
  <label class="col-md-2"></label>
  <div class="col-md-6">
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="reset" class="btn btn-danger">Reset</button>
  </div>
</div>



@section('script')
@parent

<script type="text/javascript">
  $(document).ready(function(){
    $('#divisi_id').on('change', function(){
      var divisi_id = $(this).val();

      $('#unit_kerja_id').val("");
      $('.unit_kerja_option').each(function(){
        if ($(this).data('divisi_id') != divisi_id) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    });


    setTimeout(function(){
      $('#divisi_id').trigger('change');
        var selected_unit_id = $('#unit_kerja_id').data('selected');
      $('#unit_kerja_id').val(selected_unit_id);
    }, 100);
  });
</script>

@endsection