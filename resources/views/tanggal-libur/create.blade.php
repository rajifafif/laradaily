@extends('layouts.main')

@section('title', 'Tanggal Libur')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Tambah hari libur</h3>
            </div>
            <br>
                <form action="{{ route('tbl-libur.store') }}" method="POST">
                    @include('tanggal-libur._form')
                </form>
            </div>
        </div>
          
      </div>
    <!-- /.row -->
@endsection