<br>
@csrf
<input type="hidden" name="idTimeSheet" value="{{ $timesheet->idTimeSheet }}">

<div class="form-group row">
  <label for="time_from" class="col-md-2 col-form-label text-md-right">Jam Mulai</label>
  <div class="col-md-6">
    <div class="input-group date timepicker" id="time_from" data-target-input="nearest">
      
      <input type="text" name="time_from_act" class="form-control datetimepicker-input" data-target="#time_from" placeholder="hh:mm" value="{{ old('time_from_act', 
      isset($activity) ? $activity->time_from_act->format('H:i') : ($timesheet->time_from->format('H:i') ?? '')) }}" />
      
      <div class="input-group-append" data-target="#time_from" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="far fa-clock"></i></div>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <label for="time_to" class="col-md-2 col-form-label text-md-right">Jam Selesai</label>
  <div class="col-md-6">
    <div class="input-group date timepicker" id="time_to" data-target-input="nearest">
      
      <input type="text" name="time_to_act" class="form-control datetimepicker-input" data-target="#time_to" placeholder="hh:mm" value="{{ 
      old('time_to_act', 
      isset($activity) ? $activity->time_to_act->format('H:i') : ($timesheet->time_to->format('H:i') ?? '')) }}"/>

      <div class="input-group-append" data-target="#time_to" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="far fa-clock"></i></div>
      </div>
    </div>
  </div>
</div>

<div class="form-group row">
  <label for="realisasiEmployee" class="col-md-2 col-form-label text-md-right">Rencana / Kegiatan</label>
  <div class="col-md-6">
    
    <textarea class="textarea form-control" rows="5" name="realisasiEmployee" id="realisasiEmployee" placeholder="Apa yang akan Anda lakukan ?">{{ old('realisasiEmployee', $activity->realisasiEmployee ?? '') }}</textarea>
  
  </div>
</div>

<div class="form-group row">
  <label class="col-md-2"></label>
  <div class="col-md-6">
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="reset" class="btn btn-danger">Reset</button>
  </div>
</div>