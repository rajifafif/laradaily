@extends('layouts.main')

@section('title', 'Realisasi Kegiatan')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Rencana Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
                <div class="form-group row">
                  <label class="col-md-2 col-form-label text-md-right">Rencana</label>
                  <div class="col-md-6">
                    <div class="input-group date timepicker" id="time_from" data-target-input="nearest">                      
                      <div class="form-control" readonly="readonly">
                        {{ $timesheet->rencanaEmployee }}
                      </div>                      
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 col-form-label text-md-right">Tanggal</label>
                  <div class="col-md-6">
                    <div class="input-group date timepicker" id="time_from" data-target-input="nearest">                      
                      <div class="form-control" readonly="readonly">
                        {{ $timesheet->date->format('d-m-Y') }}
                      </div>                      
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 col-form-label text-md-right">Dari</label>
                  <div class="col-md-6">
                    <div class="input-group date timepicker" id="time_from" data-target-input="nearest">                      
                      <div class="form-control" readonly="readonly">
                        {{ $timesheet->time_from->format('H:i') }}</p>
                      </div>                      
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2 col-form-label text-md-right">Sampai</label>
                  <div class="col-md-6">
                    <div class="input-group date timepicker" id="time_from" data-target-input="nearest">                      
                      <div class="form-control" readonly="readonly">
                        {{ $timesheet->time_to->format('H:i') }}
                      </div>                      
                    </div>
                  </div>
                </div>
              </div>
            </div>


        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Input Realisasi Kegiatan</h3>
            </div>
            <br>
                <form action="{{ route('realisasi-kegiatan.store') }}" method="POST">
                    @include('realisasi-kegiatan._form')
                </form>
            </div>
        </div>
          
      </div>
    </div>
    <!-- /.row -->
@endsection