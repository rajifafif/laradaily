@extends('layouts.main')

@section('title', 'Realisasi Kegiatan')

@section('content')
<div class="row">
    <div class="col-lg-12">

        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Agenda Kegiatan Minggu ini</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr class="text-center">
                            <th class="align-middle" rowspan="2">No.</th>
                            <th class="align-middle" rowspan="2">Tanggal</th>
                            <th class="align-middle" colspan="2">Waktu Rencana</th>
                            <th class="align-middle" rowspan="2">Rencana Kegiatan</th>
                            <th class="align-middle" colspan="2">Waktu Realisasi</th>
                            <th class="align-middle" rowspan="2">Realisasi Kegiatan</th>
                            <th class="align-middle" colspan="2">Komentar</th>
                            <th class="align-middle" rowspan="2">Action</th>
                            <th class="align-middle" rowspan="2">Keterangan</th>
                        </tr>
                        <tr class="text-center">
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Ka. Biro / Manajer</th>
                            <th>Direksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($timesheets as $timesheet)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $timesheet->date->format('d-m-Y')}}</td>
                            <td>{{ $timesheet->time_from->format('H:i')}}</td>
                            <td>{{ $timesheet->time_to->format('H:i')}}</td>
                            <td>{{ $timesheet->rencanaEmployee }}</td>
                            <td>{{ isset($timesheet->activity) ? $timesheet->activity->time_from_act->format('H:i') : ''}}</td>
                            <td>{{ isset($timesheet->activity) ? $timesheet->activity->time_to_act->format('H:i') : '' }}</td>
                            <td>{{ $timesheet->activity->realisasiEmployee ?? '' }}</td>
                            @if($timesheet->kabid_comments > 0)
                            <td><a href="{{ route('job-order.comment', $timesheet->idTimeSheet) }}" class="btn btn-sm btn-danger">{{ $timesheet->kabid_comments }} Komentar</a></td>
                            @else
                            <td></td>
                            @endif
                            @if($timesheet->direksi_comments > 0)
                            <td><a href="{{ route('job-order.comment', $timesheet->idTimeSheet) }}" class="btn btn-sm btn-danger">{{ $timesheet->direksi_comments }} Komentar</a></td>
                            @else
                            <td></td>
                            @endif
                            <td>
                        
                                @if($timesheet->date->startOfDay() == now()->startOfDay())               

                                @if(!isset($timesheet->activity))
                               
                                <a class="btn btn-sm btn-primary" href="{{ 'realisasi-kegiatan/'.$timesheet->idTimeSheet.'/create' }}">Input</a>
                                @endif

                                @isset($timesheet->activity)
                                <a class="btn btn-sm btn-warning" href="{{ route('realisasi-kegiatan.edit', $timesheet->activity->idActivity) }}">Edit</a>
                                @endisset
                         
                                @endif
                            
                            </td>
                            <td></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{ $timesheets->links() }}
            </div>
        </div>

    </div>
</div>
<!-- /.row -->
@endsection