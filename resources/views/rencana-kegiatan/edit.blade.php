@extends('layouts.main')

@section('title', 'Rencana Kegiatan')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Rencana Kegiatan</h3>
          </div>
          <!-- /.card-header -->
          @include('rencana-kegiatan._form')
        </div>

      </div>
    </div>
    <!-- /.row -->
@endsection