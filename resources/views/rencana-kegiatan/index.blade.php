@extends('layouts.main')

@section('title', 'Rencana Kegiatan')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Rencana Kegiatan</h3>
          </div>
          <!-- /.card-header -->
          @include('rencana-kegiatan._form')
        </div>

        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">Agenda Kegiatan Minggu Ini</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <table class="table table-striped table-hover table-responsive">
              <tr>
                <th>No.</th>
                <th>Tanggal</th>
                <th>Jam Mulai</th>
                <th>Jam Selesai</th>
                <th>Tanggal Input</th>
                <th>Rencana Kegiatan</th>
                <th>Input Oleh</th>
                <th>Action</th>
              </tr>
              @foreach($timesheets as $timesheet)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $timesheet->date->format('d-m-Y') }}</td>
                <td>{{ $timesheet->time_from->format('H:i') }}</td>
                <td>{{ $timesheet->time_to->format('H:i') }}</td>
                <td>{{ $timesheet->date_submit->format('d-m-Y') }}</td>
                <td>{{ $timesheet->rencanaEmployee }}</td>
                <td>{{ $timesheet->submitBy }}</td>
                <td>
                  <a class="btn btn-sm btn-warning" href="{{ route('rencana-kegiatan.edit', $timesheet->idTimeSheet) }}">Edit</a>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <div class="card-footer">
            {{ $timesheets->links() }}
          </div>
        </div>

      </div>
    </div>
    <!-- /.row -->
@endsection