<!-- form start -->
<form role="form" method="POST" action="{{ route('rencana-kegiatan.store') }}">
  @csrf
    <div class="card-body">
        <div class="form-group row">
        <label for="date" class="col-md-2 col-form-label text-md-right">Tanggal</label>
        <div class="col-md-6">
            <input type="text" name="date" class="form-control datepickerrencana {{ isset($timesheet) ? 'edited' : '' }}" id="date" placeholder="dd-mm-yyyy" value="{{ old('date', isset($timesheet->date) ? $timesheet->date->format('d-m-Y') : '') }}">
            @error('date')
            <small class="form-text text-danger">{{ $message }}</small>
            @enderror
        </div>
        </div>
    <div class="form-group row">
      <label for="time_from" class="col-md-2 col-form-label text-md-right">Jam Mulai</label>
      <div class="col-md-6">
        <div class="input-group date timepicker" id="time_from" data-target-input="nearest">
          <input type="text" name="time_from" class="form-control datetimepicker-input" data-target="#time_from" placeholder="hh:mm" value="{{ old('time_from', isset($timesheet->time_from) ? $timesheet->time_from->format('H:i') : '') }}"/>
          <div class="input-group-append" data-target="#time_from" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="far fa-clock"></i></div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group row">
      <label for="time_to" class="col-md-2 col-form-label text-md-right">Jam Selesai</label>
      <div class="col-md-6">
        <div class="input-group date timepicker" id="time_to" data-target-input="nearest">
          <input type="text" name="time_to" class="form-control datetimepicker-input" data-target="#time_to" placeholder="hh:mm" value="{{ old('time_to', isset($timesheet->time_to) ? $timesheet->time_to->format('H:i') : '') }}"/>
          <div class="input-group-append" data-target="#time_to" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="far fa-clock"></i></div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group row">
      <label for="rencanaEmployee" class="col-md-2 col-form-label text-md-right">Rencana / Kegiatan</label>
      <div class="col-md-6">
        <textarea class="textarea form-control" rows="5" name="rencanaEmployee" id="rencanaEmployee" placeholder="Apa yang akan Anda lakukan ?">{{ old('rencanaEmployee', $timesheet->rencanaEmployee ?? '') }}</textarea>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-md-2"></label>
      <div class="col-md-6">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-danger">Reset</button>
      </div>
    </div>
  </div>
  <!-- /.card-body -->
</form>