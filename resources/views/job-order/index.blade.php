@extends('layouts.main')

@section('title', 'Rencana Kegiatan')



@section('content')
<div class="row">
    <div class="col-lg-12">

        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Agenda Kegiatan Minggu Ini</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-bordered">
                    <tr>
                        <th>No.</th>
                        <th>Unit Kerja</th>
                        <th>Nama</th>
                    </tr>
                    @php
                    $nomer = 1;
                    @endphp

                    @foreach($unit_kerjas as $unit_kerja)

                    @php
                    $total_user = $unit_kerja->users->count();
                    @endphp

                    @if($total_user) {{--Jika unit kerja PUNYA user / employe--}}
                    @foreach($unit_kerja->users as $user)
                    <tr>
                        <td>{{ $nomer++ }}</td>
                        @if($loop->first)
                        <td rowspan="{{ $total_user }}">{{$unit_kerja->unitKerja }}</td>
                        @endif
                        <td><a href="{{ route('job-order.show', $user->id) }}">{{ $user->name }}</a></td>
                    </tr>
                    @endforeach
                    @else {{--jika unitkerja TIDAK PUNYA user--}}
                    <tr>
                        <td>{{ $nomer++ }}</td>
                        <td>{{ $unit_kerja->unitKerja }}</td>
                        <td>-</td>
                    </tr>
                    @endif

                    @endforeach


                    {{--
              @foreach($timesheets as $timesheet)
              <tr>
                <td>{{ $loop->iteration }}</td>
                    <td>{{ $timesheet->date->format('d-m-Y')}}</td>
                    <td>{{ $timesheet->time_from->format('H:i')}}</td>
                    <td>{{ $timesheet->time_to->format('H:i')}}</td>
                    <td>{{ $timesheet->rencanaEmployee }}</td>
                    <td>{{ isset($timesheet->activity) ? $timesheet->activity->time_from_act->format('H:i') : ''}}</td>
                    <td>{{ isset($timesheet->activity) ? $timesheet->activity->time_to_act->format('H:i') : '' }}</td>
                    <td>{{ $timesheet->activity->realisasiEmployee ?? '' }}</td>
                    <td>{{ $timesheet->activity->commentAtasan ?? ''}}</td>
                    <td>{{ $timesheet->activity->commentBoss ?? ''}}</td>
                    <td>
                        @if(!isset($timesheet->activity))
                        <a class="btn btn-sm btn-primary" href="{{ 'realisasi-kegiatan/'.$timesheet->idTimeSheet.'/create' }}">Input</a>
                        @endif

                        @isset($timesheet->activity)
                        <a class="btn btn-sm btn-warning" href="{{ route('realisasi-kegiatan.edit', $timesheet->activity->idActivity) }}">Edit</a>
                        @endisset
                    </td>
                    </tr>
                    @endforeach
                    --}}
                </table>
            </div>
            <div class="card-footer">
                {{-- $timesheets->links() --}}
            </div>
        </div>

    </div>
</div>
<!-- /.row -->
@endsection