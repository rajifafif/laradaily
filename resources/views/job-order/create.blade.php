@extends('layouts.main')

@section('title', 'Realisasi Kegiatan')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Realisasi Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <p>Rencana : {{ $timesheet->rencanaEmployee }}</p>
                <p>Dari : {{ $timesheet->time_from }}</p>
                <p>Sampai : {{ $timesheet->time_to }}</p>
                <form action="{{ route('realisasi-kegiatan.store') }}" method="POST">
                    @include('realisasi-kegiatan._form')
                </form>
            </div>
        </div>
          
      </div>
    </div>
    <!-- /.row -->
@endsection