@extends('layouts.main')

@section('title', 'Realisasi Kegiatan')

@section('title', 'Rencana-kegiatan.store')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-success">
            <div class="row">
                <div class="col-md-3 col-xs-6">Name</div>
                <div class="col-md-3 col-xs-6">{{ $employee->name }}</div>
            </div>
        </div>
        <!--form tambah job order dari atasan -->
        <form role="form" method="POST" action="{{ route('job-order.store', $employee) }}">
        @csrf
        <div class="card-body">
            <div class="card-header">
                <h3 class="card-title">Penugasan Pekerjaan untuk  <bold>{{ $employee->name}} </bold></h3>
            </div>            
            <div class="form-group row">
                <label for="date" class="col-md-2 col-form-label text-md-right">Tanggal</label>
                <div class="col-md-6">
                    <input type="text" name="date" class="form-control datepickerrencana" id="date" placeholder="dd-mm-yyyy">
                </div>
            </div>
            <div class="form-group row">
                <label for="time_from" class="col-md-2 col-form-label text-md-right">Jam Mulai</label>
                <div class="col-md-6">
                    <div class="input-group date timepicker" id="time_from" data-target-input="nearest">
                        <input type="text" name="time_from" class="form-control datetimepicker-input" data-target="#time_from" placeholder="hh:mm" />
                        <div class="input-group-append" data-target="#time_from" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="time_to" class="col-md-2 col-form-label text-md-right">Jam Selesai</label>
                <div class="col-md-6">
                    <div class="input-group date timepicker" id="time_to" data-target-input="nearest">
                        <input type="text" name="time_to" class="form-control datetimepicker-input" data-target="#time_to" placeholder="hh:mm" />
                        <div class="input-group-append" data-target="#time_to" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="rencanaEmployee" class="col-md-2 col-form-label text-md-right">Rencana / Kegiatan</label>
                <div class="col-md-6">
                    <textarea class="textarea form-control" rows="5" name="rencanaEmployee" id="rencanaEmployee" placeholder="Apa yang akan Anda lakukan ?"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2"></label>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
            </div>
        </div>
        </form>
        
        <!-- Agenda kegiatan -->
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Agenda Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-bordered table-striped table-hover table-responsive">
                    <thead>
                        <tr class="text-center">
                            <th class="align-middle" rowspan="2">No.</th>
                            <th class="align-middle" rowspan="2">Hari</th>
                            <th class="align-middle" rowspan="2" width="130px">Tanggal</th>
                            <th class="align-middle" colspan="2">Waktu Rencana</th>
                            <th class="align-middle" rowspan="2">Rencana Kegiatan</th>
                            <th class="align-middle" rowspan="2">Kode Input</th>
                            <th class="align-middle" colspan="2">Waktu Realisasi</th>
                            <th class="align-middle" rowspan="2">Realisasi Kegiatan</th>
                            <th class="align-middle" rowspan="2">Komentar</th>
                            <th class="align-middle" rowspan="2"> Penilaian
                            </th>
                        </tr>
                        <tr class="text-center">
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($timesheets as $timesheet)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $timesheet->date->translatedFormat('l') }}</td>
                            <td>{{ $timesheet->date->format('d-m-Y') }}</td>
                            <td>{{ $timesheet->time_from->format('H:i')}}</td>
                            <td>{{ $timesheet->time_to->format('H:i')}}</td>
                            <td>{{ $timesheet->rencanaEmployee }}</td>
                            <td>{{ $timesheet->submitBy }}</td>
                            <td>{{ isset($timesheet->activity) ? $timesheet->activity->time_from_act->format('H:i') : ''}}</td>
                            <td>{{ isset($timesheet->activity) ? $timesheet->activity->time_to_act->format('H:i') : '' }}</td>
                            <td>{{ $timesheet->activity->realisasiEmployee ?? '' }}</td>
                            <!--
                            <td><a class="btn btn-primary" href="{{ route('job-order.comment', $timesheet->idTimeSheet) }}">Komentari</td>
                            -->
                            
                            <td><a href="{{ route('job-order.comment', $timesheet->idTimeSheet) }}" class="btn btn-sm btn-danger">{{ $timesheet->kabid_comments }} Komentar</a></td>
                            <td>
                            <select class="form-control penilaian-option" name="penilaian" data-timesheet_id="{{$timesheet->idTimeSheet}}">
                                @foreach([0,1,2,3,4,5] as $nilai)
                                <option value="{{ $nilai }}" {{ $nilai == $timesheet->nilai ? 'selected' : '' }}>{{ $nilai }}</option>
                                @endforeach
                            </select>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{ $timesheets->links() }}
            </div>
        </div>

    </div>
</div>
<!-- /.row -->
@endsection

@section('script')
@parent


<script type="text/javascript">
    $(document).ready(function() {
        $('.penilaian-option').on('change', function() {
            var nilai = $(this).val();
            var timesheetID = $(this).data('timesheet_id');

            $.ajax({
                type:'POST',
                url:'/job-order/nilai',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    nilai: nilai,
                    timesheet_id: timesheetID,
                },
                success:function(status){
                    if (status) {
                        console.log('success');
                    } else {
                        console.log('failed');
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
        });
    });
</script>

@endsection