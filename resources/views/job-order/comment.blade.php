@extends('layouts.main')

@section('title', 'Komentar Timesheet')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Timesheet</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 text-bold">Rencana</div>
                            <div class="col-md-8">{{ $timesheet->rencanaEmployee }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-bold">Dari</div>
                            <div class="col-md-8">{{ $timesheet->time_from->format('d-m-Y H:i') }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-bold">Sampai</div>
                            <div class="col-md-8">{{ $timesheet->time_to->format('d-m-Y H:i') }}</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 text-bold">Realisasi</div>
                            <div class="col-md-8">{{ $timesheet->activity->realisasiEmployee ?? '' }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-bold">Dari</div>
                            <div class="col-md-8">{{ isset($timesheet->activity) ? $timesheet->activity->time_from_act->format('d-m-Y H:i') : '' }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-bold">Sampai</div>
                            <div class="col-md-8">{{ isset($timesheet->activity) ? $timesheet->activity->time_to_act->format('d-m-Y H:i') : '' }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card direct-chat direct-chat-primary card-primary">
              <div class="card-header ui-sortable-handle" style="cursor: move;">
                <h3 class="card-title">Komentar Timesheet</h3>

                <div class="card-tools">
                  <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge badge-primary">3</span> -->
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages">
                    @foreach($comments as $comment)

                        @if($comment->user_id != auth()->user()->id)
                          <!-- Message. Default to the left -->
                          <div class="direct-chat-msg">
                            <div class="direct-chat-infos clearfix">
                              <span class="direct-chat-name float-left">{{ $comment->user->name }}</span>
                              <span class="direct-chat-timestamp float-right">{{ now()->diffForHumans($comment->created_at) }}</span>
                            </div>
                            <!-- /.direct-chat-infos -->
                            <div class="avatar-circle direct-chat-img">
                                <span class="initials">{{ $comment->user->nickname }}</span>
                            </div>
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                              {{ $comment->comment }}
                            </div>
                            <!-- /.direct-chat-text -->
                          </div>
                          <!-- /.direct-chat-msg -->
                        @else
                          <!-- Message to the right -->
                          <div class="direct-chat-msg right">
                            <div class="direct-chat-infos clearfix">
                              <span class="direct-chat-name float-right">Anda</span>
                              <span class="direct-chat-timestamp float-left">{{ now()->diffForHumans($comment->created_at) }}</span>
                            </div>
                            <!-- /.direct-chat-infos -->
                            <div class="avatar-circle direct-chat-img">
                                <span class="initials">{{ $comment->user->nickname }}</span>
                            </div>
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                              {{ $comment->comment }}
                            </div>
                            <!-- /.direct-chat-text -->
                          </div>
                          <!-- /.direct-chat-msg -->
                        @endif

                    @endforeach

                </div>
                <!--/.direct-chat-messages-->

                
                <!-- /.direct-chat-pane -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <form action="{{ route('job-order.store-comment', $timesheet->idTimeSheet) }}" method="POST">
                    @csrf
                  <div class="input-group">
                    <input type="text" name="comment" placeholder="Tulis Komentar" class="form-control" autofocus>
                    <span class="input-group-append">
                      <button type="submit" class="btn btn-primary">Send</button>
                    </span>
                  </div>
                </form>
              </div>
              <!-- /.card-footer-->
            </div>
          
      </div>
    </div>
    <!-- /.row -->
@endsection