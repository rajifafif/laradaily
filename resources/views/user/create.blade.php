@extends('layouts.main')

@section('title', 'Tambah user')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create User</h3>
            </div>
            <br>
                <form action="{{ route('users.store') }}" method="POST">
                    @include('user._form')
                </form>
            </div>
        </div>
          
      </div>
    <!-- /.row -->
@endsection