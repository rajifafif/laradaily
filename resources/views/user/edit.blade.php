@extends('layouts.main')

@section('title', 'Realisasi Kegiatan')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit User</h3>
            </div>
            <br>
                <form action="{{ route('users.update', $user) }}" method="POST">
                    @method('PATCH')
                    @include('user._form')
                </form>
            </div>
        </div>
          
      </div>
    <!-- /.row -->
@endsection