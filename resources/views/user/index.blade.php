@extends('layouts.main')

@section('title', 'Users')

@section('content')
<div class="row">
    <div class="col-lg-12">

        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">List Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="p-2">
                    <div class="row">
                        <div class="col-md-4">
                            <form method="GET" id="search">
                                <div class="form-group">
                                    <input name="search" class="form-control" placeholder="Search" value="{{ old('search') }}">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-2">
                            <a href="{{ route('users.create') }}" class="btn btn-primary">Create User</a>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr class="text-center">
                            <!-- <th class="align-middle">No</th> -->
                            <th class="align-middle">#ID</th>
                            <th class="align-middle">Nama</th>
                            <th class="align-middle">Nickname</th>
                            <th class="align-middle">Username</th>
                            <th class="align-middle">Email</th>
                            <th class="align-middle">Divisi</th>
                            <th class="align-middle">Unit Kerja</th>
                            <th class="align-middle">Role</th>
                            <th class="align-middle">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <!-- <td>{{ ($users->currentPage() - 1) * 10 + $loop->iteration }}</td> -->
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->nickname }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->divisi->txtDivisi ?? '' }}</td>
                            <td>{{ $user->unitKerja->unitKerja ?? '' }}</td>
                            <td>{{ $user->roles->first()->name ?? '' }}</td>
                            <td>
                                <div class="d-flex">
                                <a class="btn btn-sm btn-primary" href="{{ route('users.edit', $user) }}"><i class="fas fa-edit"></i></a>
                                &nbsp;
                                <form id="delete-user-{{ $user->id }}" method="POST" action="{{ route('users.destroy', $user) }}" onsubmit="return confirm('Are You Sure to Delete : {{ $user->name }}')">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{ $users->appends(request()->input())->links() }}
            </div>
        </div>

    </div>
</div>
<!-- /.row -->
@endsection