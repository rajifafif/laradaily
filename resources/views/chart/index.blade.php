@extends('layouts.main')

@section('title', 'Chart')

@section('content')
<!--CHART KARYAWAN -->
<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary collapsed-card">
            <div class="card-header" data-card-widget="collapse">
                <h3 class="card-title">Daily Activity per BIRO </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool">
                        <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('chart.show') }}">
                    @csrf
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">Periode Awal</label>
                            <div class="col-md-6">
                                <input type="text" name="periode_awal" class="form-control datepicker" id="date" placeholder="dd-mm-yyyy" value="{{ old('periode_awal', request()->periode_awal ?? '') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">Periode Akhir</label>
                            <div class="col-md-6">
                                <input type="text" name="periode_akhir" class="form-control datepicker" id="date" placeholder="dd-mm-yyyy" value="{{ old('periode_akhir', request()->periode_akhir ?? '') }}">
                            </div>
                        </div>

                        <!-- SELECT DIVISI -->
                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">Divisi</label>
                            <div class="col-md-6">
                                <select name="divisi_id" id="divisi_id" class="form-control">
                                    @if(count($divisis) > 1)
                                    <option value="all">Semua</option>
                                    @endif
                                    @foreach($divisis as $divisi)
                                    <option value="{{ $divisi->idDivisi }}" {{ ($divisi->idDivisi == request()->divisi_id) ? 'selected' : '' }}>{{ $divisi->txtDivisi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">UnitKerja</label>
                            <div class="col-md-6">
                                <select name="unit_kerja_id" id="unit_kerja_id" class="form-control">
                                    <option value="all">Semua</option>
                                    @foreach($divisis as $divisi)
                                    @foreach($divisi->unit_kerjas as $unit_kerja)
                                    <option class="unit_kerja_option" 
                                            data-divisi_id="{{ $divisi->idDivisi }}" 
                                            value="{{ $unit_kerja->idUnitKerja }}" 
                                            {{ ($unit_kerja->idUnitKerja == request()->unit_kerja_id) ? 'selected' : '' }} 
                                            style="display: none;">
                                                {{ $unit_kerja->unitKerja }}</option>
                                    @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2"></label>
                            <div class="col-md-6">
                                <button type="submit" name="origin" value="biro" class="btn btn-success"><i class="fas fa-eye"></i> Lihat Chart</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>

        @if (isset($total_timesheets) && request()->origin == 'biro')
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Chart

                    <!--@if(request()->unit_kerja_id == 'all') Semua Divisi @else {{ $unit_kerja->unitKerja }} @endif 
            -->
                    Periode {{ request()->periode_awal }} - {{ request()->periode_akhir }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="chart-container" style="position: relative; width: 80%;">
                    <canvas id="chart"></canvas>
                </div>
            </div>

        </div>
        @endif

    </div>
</div>
<!-- CHART Divisi -->

<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary collapsed-card">
            <div class="card-header" data-card-widget="collapse">
                <h3 class="card-title">Daily Activity per Divisi </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool">
                        <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- form start -->
                <form role="form" method="POST" action="{{ route('chart.show') }}">
                    @csrf
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">Periode Awal</label>
                            <div class="col-md-6">
                                <input type="text" name="periode_awal" class="form-control datepicker" id="date" placeholder="dd-mm-yyyy" value="{{ old('periode_awal', request()->periode_awal ?? '') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">Periode Akhir</label>
                            <div class="col-md-6">
                                <input type="text" name="periode_akhir" class="form-control datepicker" id="date" placeholder="dd-mm-yyyy" value="{{ old('periode_akhir', request()->periode_akhir ?? '') }}">
                            </div>
                        </div>

                        {{--
                        <!-- SELECT DIVISI -->
                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label text-md-right">Divisi</label>
                            <div class="col-md-6">
                                <select name="divisi_id" id="divisi_id" class="form-control">
                                    @if(count($divisis) > 1)
                                    <option value="">Pilih Divisi</option>
                                    @endif
                                    @foreach($divisis as $divisi)
                                    <option value="{{ $divisi->idDivisi }}" {{ $divisi->idDivisi == old('divisi_id', $request->divisi_id ?? '') ? 'selected' : '' }}>{{ $divisi->txtDivisi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        --}}
                        <div class="form-group row">
                            <label class="col-md-2"></label>
                            <div class="col-md-6">
                                <button type="submit" name="origin" value="divisi" class="btn btn-success"><i class="fas fa-eye"></i> Lihat Chart</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>

        @if (isset($total_timesheets) && request()->origin == 'divisi')
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Chart

                    <!--@if(request()->unit_kerja_id == 'all') Semua Divisi @else {{ $unit_kerja->unitKerja }} @endif 
            -->
                    Periode {{ request()->periode_awal }} - {{ request()->periode_akhir }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="chart-container" style="position: relative; width: 80%;">
                    <canvas id="chart"></canvas>
                </div>
            </div>

        </div>
        @endif

    </div>
</div>

@endsection

@section('script')
@parent


<script type="text/javascript">
    $(document).ready(function() {
        $('#divisi_id').on('change', function() {
            var divisi_id = $(this).val();

            $('#unit_kerja_id').val("all");
            $('.unit_kerja_option').each(function() {
                if ($(this).data('divisi_id') != divisi_id) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });


        setTimeout(function() {
            $('#divisi_id').trigger('change');
        }, 100);
    });
</script>

@isset($total_timesheets)
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
<script>
    var ctx = document.getElementById('chart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        @if (isset($total_timesheets) && request()->origin == 'biro')
        data: {
            labels: [
                @foreach($total_timesheets as $total_timesheet)
                '{{ $total_timesheet->label }}',
                @endforeach
            ],
            datasets: [{
                label: '# Timesheet',
                data: [
                    @foreach($total_timesheets as $total_timesheet)
                        {{ $total_timesheet -> total_timesheet }}, 
                    @endforeach
                ],
                backgroundColor: [
                    @foreach($total_timesheets as $total_timesheet)
                    'rgba(0, 123, 255, 0.7)',
                    @endforeach
                ],
                borderColor: [
                    @foreach($total_timesheets as $total_timesheet)
                    'rgba(0, 123, 255, 1)',
                    @endforeach
                ],
                borderWidth: 1
            }]
        },
        @elseif(isset($total_timesheets) && request()->origin == 'divisi')
        data: {!! json_encode($total_timesheets) !!},
        @endif
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>
@endisset
@endsection