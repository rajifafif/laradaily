@extends('layouts.main')

@section('title', '')

@section('content')
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">Lihat Report</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('report.show') }}">
              @csrf
              <div class="card-body">        
                <div class="form-group row">
                  <label for="date" class="col-md-2 col-form-label text-md-right">Periode Awal</label>
                  <div class="col-md-6">
                    <input type="text" name="periode_awal" class="form-control datepicker" id="date" placeholder="dd-mm-yyyy" value="{{ old('periode_awal', $request->periode_awal ?? '') }}">
                  </div>
                </div>
                
                <div class="form-group row">
                  <label for="date" class="col-md-2 col-form-label text-md-right">Periode Akhir</label>
                  <div class="col-md-6">
                    <input type="text" name="periode_akhir" class="form-control datepicker" id="date" placeholder="dd-mm-yyyy" value="{{ old('periode_akhir', $request->periode_akhir ?? '') }}">
                  </div>
                </div>
                
                <!-- SELECT DIVISI -->
                <div class="form-group row">
                  <label for="date" class="col-md-2 col-form-label text-md-right">Divisi</label>
                  <div class="col-md-6">
                    <select name="divisi_id" id="divisi_id" class="form-control">
                      @if(count($divisis) > 1)
                        <option value="">Pilih Divisi</option>
                      @endif
                      @foreach($divisis as $divisi)
                        <option value="{{ $divisi->idDivisi }}" {{ $divisi->idDivisi == old('divisi_id', $request->divisi_id ?? '') ? 'selected' : '' }}>{{ $divisi->txtDivisi }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <!-- SELECT UNIT -->
                <div class="form-group row">
                  <label for="date" class="col-md-2 col-form-label text-md-right">UnitKerja</label>
                  <div class="col-md-6">
                    <select name="unit_kerja_id" id="unit_kerja_id" class="form-control" data-selected="{{old('unit_kerja_id', $request->unit_kerja_id ?? '')}}">
                      @if(count($divisis) > 1)
                        <option value="">Pilih Unit Kerja</option>
                      @endif
                      @foreach($divisis as $divisi)
                      @foreach($divisi->unit_kerjas as $unit_kerja)
                        <option class="unit_kerja_option" 
                                data-divisi_id="{{ $divisi->idDivisi }}" 
                                value="{{ $unit_kerja->idUnitKerja }}" 
                                {{ $unit_kerja->idUnitKerja == old('unit_kerja_id', $request->unit_kerja_id ?? '') ? 'selected' : '' }} 
                                style="display: none;">
                            {{ $unit_kerja->unitKerja }}
                        </option>
                      @endforeach
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-2"></label>
                  <div class="col-md-6">
                    <button type="submit" name="output" value="print" class="btn btn-primary"><i class="fas fa-print"></i> Print</button>
                    <button type="submit" name="output" value="web" class="btn btn-success"><i class="fas fa-eye"></i> View</button>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </form>
          </div>
            
        </div>
        @isset($reports)
        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">Hasil Report</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body overflow-auto p-0">
            @include('report/_table')
          </div>
        </div>
        @endisset
      </div>
    </div>
    <!-- /.row -->
@endsection


@section('script')
@parent

<script type="text/javascript">
  $(document).ready(function(){
    $('#divisi_id').on('change', function(){
      var divisi_id = $(this).val();

      $('#unit_kerja_id').val("");
      $('.unit_kerja_option').each(function(){
        if ($(this).data('divisi_id') != divisi_id) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    });


    setTimeout(function(){
      $('#divisi_id').trigger('change');
        var selected_unit_id = $('#unit_kerja_id').data('selected');
      $('#unit_kerja_id').val(selected_unit_id);
    }, 100);
  });
</script>

@endsection