<table class="table table-bordered table-report">
    <thead>
        <tr>
            <th rowspan="2" style="vertical-align: middle; text-align: center;">Nama</th>
            @foreach($table_date as $year => $date_in_year)
            @foreach($date_in_year as $month => $date_in_month)
            <th colspan="{{ count($date_in_year[$month]) }}" style="vertical-align: middle; text-align: center;">{{ $month }}</th>
            @endforeach
            @endforeach
            <th colspan="2" style="vertical-align: middle; text-align: center;">Jumlah</th>
            <th colspan="2" style="vertical-align: middle; text-align: center;">Pelaksanaan pemotongan</th>
            <!--
        <th rowspan="2" style="vertical-align: middle; text-align: center;">Arahan atasan langsung </th>
        -->
        </tr>
        <tr>
            @foreach($table_date as $year => $date_in_year)
            @foreach($date_in_year as $month => $date_in_month)
            @foreach($date_in_month as $date => $isWeekday)
            <th>{{ $date }}</th>
            @endforeach
            @endforeach
            @endforeach
            <th style="white-space: nowrap">Di Isi</th>
            <th style="white-space: nowrap">Tidak</th>
            <th>Ya</th>
            <th>Tidak</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reports as $report)
        @php
        $jml_ya = 0;
        $jml_tidak = 0;
        @endphp
        <tr>
            <td style="white-space: nowrap; font-weight: bold">{{ $report->name }}</td>
            @foreach($table_date as $year => $date_in_year)
            @foreach($date_in_year as $month => $date_in_month)
            @foreach($date_in_month as $date => $isWeekday)

            @php
            $isHasTimesheet = isset($report->timesheet_in_array[$year][$month][$date]);

            if($isHasTimesheet && $isWeekday) {
            $jml_ya++;
            } else if(!$isHasTimesheet && $isWeekday) {
            $jml_tidak++;
            }
            @endphp

            <th {{ !$isWeekday ? 'style=background:red' : '' }}>{{ $isHasTimesheet ? 'V' : '' }} {{ !$isHasTimesheet&&$isWeekday ? '-' : ''}}</th>
            @endforeach
            @endforeach
            @endforeach
            <th>{{ $jml_ya }}</th>
            <th>{{ $jml_tidak }}</th>
            <th>Ya</th>
            <th>Tidak</th>
            <!--
            <th>..........</th>
            -->
        </tr>
        @endforeach
    </tbody>
</table>