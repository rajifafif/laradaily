@extends('layouts.print')
@section('content')
    <h4 align="center">  
       <img src="{{asset('img/wp.png') }}" alt=""/ >
        <br>
        LAPORAN  DAILY ACTIVITY PERIODE : {{ request()->periode_awal }} - {{ request()->periode_akhir }}
        <br>
        <b>{{ strtoupper($selected_unit_kerja->unitKerja) }}, </b>        
        {{ strtoupper($selected_ka_biro->divisi->txtDivisi) }}
        <br>        
    </h4>
  @include('report._table')
    <h4> Komentar atasan langsung :</h4>
    <a> .................................................................................................................................................</a>
   <h3> 
        <table width="100%">
            <tr>        
                <td width="25%"></td>
                <td width="25%"></td>
                <td width="25%"></td>
                <td width="25%">
                    <a>Jakarta, {{ now()->translatedFormat('d F Y')}}</a>
                    <br>    
                    <a> Dibuat oleh         ,</a>                
                    <br>
                    <br>
                    <br>
                    <div align="center">
                    <a><b><u> {{ $selected_ka_biro->name }} </u> </b></a>  
                    <h4>Ka.Biro/GM/Manager {{ $selected_unit_kerja->unitKerja }}</h4>            
                    </div>
                </td>
            </tr>
        </table>
    </h3>        
    <a> 
        <u> Generate by Sistem Daily Activity </u>
        <br>
        Dicetak pada :
        <b>
            {{ now()->translatedFormat('d F Y h:i:s A')}}
        </b>
    </a>
@endsection