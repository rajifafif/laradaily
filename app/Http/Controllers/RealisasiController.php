<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Timesheet;
use Illuminate\Support\Facades\Validator;

class RealisasiController extends Controller
{
    
//    protected function validator($request)
//    {
//        $validator = Validator::make($request->all(), [
//            'idTimeSheet' => 'required',
//            'time_act_from' => 'required',
//            'time_act_to' => 'required',
//            'realisasiEmployee' => 'required',
//        ]);
//        
//        return $validator;
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $timesheets = Timesheet::with(['activity'])->whereHas('employee', function($q){
                    $q->where('idEmployee', auth()->user()->id);
                })
                ->withCount([
                    'comments as kabid_comments' => function($query) {
                        $query->whereHas('user', function($user){
                            $user->whereHas('roles', function($role){
                                $role->where('name', 'KaBiro');
                            });
                        });
                    },
                    'comments as direksi_comments' => function($query) {
                        $query->whereHas('user', function($user){
                            $user->whereHas('roles', function($role){
                                $role->where('name', 'Direksi');
                            });
                        });
                    },
                ])
                ->orderBy('date', 'DESC')
                ->orderBy('time_from', 'DESC')
            ->paginate(10);
        // dd($timesheets->toArray());
        return view('realisasi-kegiatan.index', ['timesheets' => $timesheets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_timesheet)
    {
        $timesheet = Timesheet::where('idTimeSheet', $id_timesheet)->where('idEmployee', auth()->user()->id)->firstOrFail();

        //Validator Create Realisasi Hanya di hari H 
        if ($timesheet->date->startOfDay() != now()->startOfDay()) {
            return abort(404);
        }

        return view('realisasi-kegiatan.create', compact('timesheet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activity = Activity::updateOrCreate([
            'idTimeSheet' => $request->idTimeSheet,
        ], $request->all());
        
        if (!$activity) {
            return redirect()->back()->withInput();
        }
        
        return redirect(route('realisasi-kegiatan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Activity::whereHas('timesheet', function($q) {
            $q->where('idEmployee', auth()->user()->id);
        })->where('idActivity', $id)->firstOrFail();

        $timesheet = $activity->timesheet;

        //Validator Create Realisasi Hanya di hari H  
        if ($timesheet->date->startOfDay() != now()->startOfDay()) {
            return abort(404);
        }

        return view('realisasi-kegiatan.edit', compact('timesheet', 'activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity = Activity::findOrFail($id)->update($request->all());

        if (!$activity) {
            return redirect()->back()->withInput();
        }
        
        return redirect(route('realisasi-kegiatan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
