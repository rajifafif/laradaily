<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UnitKerja;
use App\Models\Timesheet;
use Carbon\Carbon;

class JobOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $unit_kerjas = UnitKerja::with(['users' => function($employees) use ($user){
            if ($user->hasRole(['KaBiro', 'Direksi', 'Manager', 'Admin','GM'])) {
                
                if ($user->hasRole('Direksi')) {
                    $employees = $employees->role(['Staff','KaBiro','Direksi','Manager','GM'])->active();
                    
                    $employees = $employees->whereNotIn('id', [1,2])->active();//Hide ID employee 2 atasnama Pak Milfan dan Pak EKO
                    
                    //$employees = $employees->hasunitKerja($user->unit_kerja_id)->role(['Direksi'])->active();
                    //$employees = $employees->hasDivisi($user->divisi_id);
                } else if ($user->hasRole('KaBiro')) {
                    $employees = $employees->hasUnitKerja($user->unit_kerja_id)->role(['Staff'])->active();
                } else if ($user->hasRole('GM')) {
                    $employees = $employees->hasDivisi($user->divisi_id)->role(['Staff','Manager'])->active();    
                } else if ($user->hasRole('Manager')) {
                    $employees = $employees->hasUnitKerja($user->unit_kerja_id)->role(['Staff'])->active();
                }
                //$employees = $employees->role(['Staff','Direksi']);
                $employees = $employees->active(); // Hanya tampilkan data staff yang aktif
                
            } else { 
                $employees = $employees->where('id', false); // just return empty if not Kabid or Direksi
            }
        }])->get();

        return view('job-order.index', compact('unit_kerjas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $employee_id)
    {
        $user = auth()->user();
        $data = $request->all();
        $data['date'] = Carbon::parse($request->date)->format('Y-m-d');
        $data['time_from'] = Carbon::parse($request->time_from)->format('H:i:s');
        $data['time_to'] = Carbon::parse($request->time_to)->format('H:i:s');
        $data['idEmployee'] = $employee_id;
        $data['submitBy'] = $user->nickname;

        $time_sheet = Timesheet::create($data);

        if ($time_sheet) {
            return redirect(route('job-order.show', $employee_id));
        }

        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = auth()->user();
        $employee = new User();

        if ($user->hasRole(['KaBiro', 'Direksi', 'Admin', 'Manager','GM'])) {
            
            if ($user->hasRole('Direksi')) {
                $employees = $employee ->hasDivisi($user->divisi_id);
            } else if ($user->hasRole(['KaBiro', 'Manager'])) {
                $employee = $employee->hasUnitKerja($user->unit_kerja_id);
            }else if ($user->hasRole(['GM'])) {
                $employee = $employee->hasDivisi($user->divisi_id);
            }
            
            $employee = $employee->role(['Staff','KaBiro','Direksi','Manager','GM'])->active(); // Hanya tampilkan data staff yang aktif
        } else {
            $employee = $employee->where('id', false); // just return empty if not Kabid or Direksi or Admin
        }

        $employee = $employee->where('id', $user_id)->firstOrFail();
        
        $timesheets = $employee->timesheets()
                        ->orderBy('date', 'DESC')
                        ->orderBy('time_from', 'DESC')
                        ->paginate();

        return view('job-order.employee_timesheets', compact('employee', 'timesheets'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment($timesheet_id)
    {
        $timesheet = Timesheet::with('comments')
            ->whereHas('user', function($q){
                $user = auth()->user();

                if ($user->hasRole('Staff')) {
                    $q->where('id', $user->id);
                } else if ($user->hasRole(['KaBiro', 'Manager'])){
                    $q->hasUnitKerja($user->unit_kerja_id);
                } else if ($user->hasRole('Direksi')){
                    //$q->hasDivisi($user->unit_kerja_id);
                } else if ($user->hasRole('Manager')){
                    $q->hasDivisi($user->unit_kerja_id);
                }  else if ($user->hasRole('GM')){
                    $q->hasDivisi($user->divisi_id);
                }  else {
                    $q->where('id', false);
                }
            })
            ->findOrFail($timesheet_id);
        $comments = $timesheet->comments;

        return view('job-order.comment', compact('timesheet', 'comments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeComment(Request $request, $timesheet_id)
    {
        $timesheet = Timesheet::findOrFail($timesheet_id);
        $comment = $timesheet->comments()->create([
            'idTimeSheet' => $timesheet_id,
            'user_id' => auth()->user()->id,
            'comment' => $request->comment,
        ]);

        if ($comment) {
            return redirect(route('job-order.store-comment', $timesheet_id));
        } else {
            return redirect()->back()->withInput();
        }
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addNilai(Request $request)
    {
        if (auth()->user()->hasRole(['Admin', 'Direksi', 'KaBiro','Manager','GM'])) {
            $timesheet = Timesheet::findOrFail($request->timesheet_id);
            $timesheet = $timesheet->update(['nilai' => $request->nilai]);
            return response()->json($timesheet);
        }

        return false;
    }
}
