<?php

namespace App\Http\Controllers;

use App\Models\User;
use Spatie\Permission\Models\Role;
use App\Models\Divisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with(['divisi', 'unitKerja']);
        
        if ($request->has('search')) {
            $users->where('name', 'like', '%'.$request->search.'%');
            $users->orWhere('nickname', 'like', '%'.$request->search.'%');
            $users->orWhere('username', 'like', '%'.$request->search.'%');
        }
        $users = $users->paginate(10);

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisis = Divisi::with('unit_kerjas')->get();
        
        $roles = Role::get();

        return view('user.create', compact('divisis', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role' => 'required',
        ]);

        $user = new User();
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = $user->create($data);

        if ($user) {
            $user->assignRole($request->role);
            return redirect(route('users.index'));
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return redirect(route('users.edit', $user));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $divisis = Divisi::with('unit_kerjas')->get();
        
        $roles = Role::get();

        return view('user.edit', compact('user', 'divisis', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'username' => 'required|unique:users,username,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
            // 'password' => 'required',
            'role' => 'required',
        ]);

        $data = $request->all();
        //Hash / Delete Data Password Agar tidak kereset
        if ($data['password'] != '') {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        
        $user->syncRoles($data['role']);

        $user->update($data);

        if ($user) {
            return redirect(route('users.index'));
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back();
    }
}
