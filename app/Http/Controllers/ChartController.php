<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Divisi;
use App\Models\Timesheet;
use App\Models\UnitKerja;
use Carbon\Carbon;

class ChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $divisis USED IN DROPDOWN
        $user = auth()->user();
        $divisis = Divisi::with(['unit_kerjas' => function($q) use($user){
            if ($user->hasRole(['KaBiro', 'Manager','GM'])) {
                return $q->where('idUnitKerja', $user->unit_kerja_id);
            }
        }]);

        $user = auth()->user();
        if ($user->hasRole(['KaBiro', 'Manager','GM'])) {
            $divisis = $divisis->where('idDivisi', $user->divisi_id);
        }

        $divisis = $divisis->get();
        return view('chart.index', compact('divisis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $date_start = Carbon::parse($request->periode_awal)->format('Y-m-d');
        $date_end = Carbon::parse($request->periode_akhir)->format('Y-m-d');

        $user = auth()->user();
        $unit_kerja = null;

        //SEMUA DIVISI SEMUA UNIT KERJA
        if ($request->origin == 'biro' && $request->divisi_id == 'all' && $request->unit_kerja_id == 'all') {
            $total_timesheets = Timesheet::whereBetween('date', [$date_start, $date_end])
                                    ->select([
                                        'unitKerja.IdUnitKerja',
                                        'unitKerja.unitKerja as label',
                                        DB::raw('count(unitKerja.IdUnitKerja) as total_timesheet')
                                    ])
                                    ->leftJoin('users', 'users.id', 'timeSheet.idEmployee')
                                    ->leftJoin('unitKerja', 'unitKerja.IdUnitKerja', 'users.unit_kerja_id')
                                    ->groupBy(['unitKerja.IdUnitKerja', 'unitKerja.unitKerja']);

            if ($user->hasRole('KaBiro')) {
                $total_timesheets = $total_timesheets->where('users.divisi_id', $user->divisi_id);
            }
                        
            $total_timesheets = $total_timesheets->get();
        }

        //UNIT KERJA PER DIVISI TERTENTU
        if ($request->origin == 'biro' && $request->divisi_id != 'all' && $request->unit_kerja_id == 'all') {
            $total_timesheets = Timesheet::whereBetween('date', [$date_start, $date_end])
                                    ->select([
                                        'unitKerja.IdUnitKerja',
                                        'unitKerja.unitKerja as label',
                                        DB::raw('count(unitKerja.IdUnitKerja) as total_timesheet')
                                    ])
                                    ->leftJoin('users', 'users.id', 'timeSheet.idEmployee')
                                    ->leftJoin('unitKerja', 'unitKerja.IdUnitKerja', 'users.unit_kerja_id')
                                    ->where('users.divisi_id', $request->divisi_id)
                                    ->groupBy(['unitKerja.IdUnitKerja', 'unitKerja.unitKerja']);

            if ($user->hasRole('KaBiro')) {
                $total_timesheets = $total_timesheets->where('users.divisi_id', $user->divisi_id);
            }
                        
            $total_timesheets = $total_timesheets->get();
        }

        //EMPLOYEE PER UNIT KERJA TERTENTU
        if ($request->origin == 'biro' && $request->divisi_id != 'all' && $request->unit_kerja_id != 'all') {
            $total_timesheets = Timesheet::whereBetween('date', [$date_start, $date_end])
                                    ->select([
                                        'users.id',
                                        'users.name as label',
                                        DB::raw('count(users.id) as total_timesheet')
                                    ])
                                    ->leftJoin('users', 'users.id', 'timeSheet.idEmployee')
                                    ->where('users.unit_kerja_id', $request->unit_kerja_id)
                                    ->groupBy(['users.id', 'users.name'])
                                    ->get();

            $unit_kerja = UnitKerja::find($request->unit_kerja_id);
        }

        //PER DIVISI
        if ($request->origin == 'divisi'){
            // $total_timesheets = Timesheet::whereBetween('date', [$date_start, $date_end])
            //                         ->select([
            //                             'divisi.idDivisi',
            //                             'divisi.txtDivisi as label',
            //                             DB::raw('count(divisi.idDivisi) as total_timesheet')
            //                         ])
            //                         ->leftJoin('users', 'users.id', 'timeSheet.idEmployee')
            //                         ->leftJoin('divisi', 'users.divisi_id', 'divisi.idDivisi')
            //                         ->groupBy(['divisi.idDivisi', 'divisi.txtDivisi'])
            //                         ->get();

            $total_timesheets = $this->getChartDataPerDivisi($date_start, $date_end);
        }


        // $divisis USED IN DROPDOWN
        $divisis = Divisi::with('unit_kerjas');
        if ($user->hasRole('KaBiro')) {
            $divisis = $divisis->where('idDivisi', $user->divisi_id);
        }
        $divisis = $divisis->get();

        return view('chart.index', compact('total_timesheets', 'divisis', 'unit_kerja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getChartDataPerDivisi($start_date, $end_date)
    {
        $total_timesheets = Timesheet::whereBetween('date', [$start_date, $end_date])
                                ->select([
                                    'divisi.idDivisi',
                                    'divisi.txtDivisi as divisi',
                                    DB::raw('count(divisi.idDivisi) as total_timesheet'),
                                    DB::raw('month(timeSheet.date) as month'),
                                    DB::raw('year(timeSheet.date) as year')
                                    ])
                                ->leftJoin('users', 'users.id', 'timeSheet.idEmployee')
                                ->leftJoin('divisi', 'users.divisi_id', 'divisi.idDivisi')
                                ->groupBy([
                                    'divisi.idDivisi', 
                                    DB::raw('month(timeSheet.date)'),
                                    DB::raw('year(timeSheet.date)'),
                                    ])
                                ->get();
        // dd($total_timesheets->toArray());

        //Map Datat Timesheet per Divisi
        $new_total_timesheets = [];
        $month_indexes = [];
        foreach ($total_timesheets as $total_timesheet) {
            $temp = $total_timesheet;
            $new_total_timesheets[$total_timesheet->idDivisi]['idDivisi'] = $total_timesheet->idDivisi;
            $new_total_timesheets[$total_timesheet->idDivisi]['divisi'] = $total_timesheet->divisi;
            $new_total_timesheets[$total_timesheet->idDivisi]['total'][$total_timesheet->year . '-' . sprintf("%02d", $total_timesheet->month)] = $total_timesheet->total_timesheet;

            $month_indexes[] = $total_timesheet->year . '-' . sprintf("%02d", $total_timesheet->month);
        }
        // dd($new_total_timesheets);

        //map all months for label
        asort($month_indexes);
        $month_indexes = array_values(array_unique($month_indexes));
        
        //Data Warna Chart Bar Per Divisi
        $warna_bar = [
            'Kantor Pusat' => 'red',
        ];

        //Map Chart Data for chartjs
        $chart_data = [];
        foreach ($month_indexes as $month_index) {
            $chart_data['labels'][] = Carbon::parse($month_index.'-01')->translatedFormat('F Y');
        }
        foreach ($new_total_timesheets as $idDivisi => $timesheet_divisi) {
            $divisi_name = $timesheet_divisi['divisi'];

            $temp_divisi['label'] = $divisi_name;
            $temp_divisi['backgroundColor'] = $warna_bar[$divisi_name] ?? '#eee' ;
            $temp_divisi['data'] = [];
            foreach ($month_indexes as $month_index) {
                if (isset($timesheet_divisi['total'][$month_index])) {
                    $temp_divisi['data'][] = $timesheet_divisi['total'][$month_index];
                } else {
                    $temp_divisi['data'][] = 0; //0 Pada bulan $month_index jika tidak punya data
                }
            }

            $chart_data['datasets'][] = $temp_divisi;
        }

        // dd(json_encode($chart_data));
        return $chart_data;
    }
}
