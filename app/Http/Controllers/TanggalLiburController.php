<?php

namespace App\Http\Controllers;

use App\Models\TanggalLibur;
use Illuminate\Http\Request;

class TanggalLiburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tgl_libur = TanggalLibur:: all();
        //dd($tgl_libur);
        //URL /tanggal-libur
        // TAMPILKAN SEMUA TANGGAL LIBUR
        return view('tanggal-libur.index', ['TanggalLibur' => $tgl_libur]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // URL /tanggal-libur/create
        // TAMPILKAN FORM PEMBUATAN TANGGAL LIBUR
        // return view('file.edit.tanggal.libur');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //PROSES SIMPAN TANGGAL LIBUR BARU
        // return redirect(route('tanggal-libur.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TanggalLibur  $tanggalLibur
     * @return \Illuminate\Http\Response
     */
    public function show(TanggalLibur $tanggalLibur)
    {
        // URL /tanggal-libur/123 {123 = Id Tanggal Libur}
        // BISA DI SKIP
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TanggalLibur  $tanggalLibur
     * @return \Illuminate\Http\Response
     */
    public function edit(TanggalLibur $tanggalLibur)
    {
        // URL /tanggal-libur/123/edit {123 = Id Tanggal Libur}
        // TAMPILKAN FORM PEMBUATAN TANGGAL LIBUR
        // return view('file.edit.tanggal.linur', compact('tanggalLibur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TanggalLibur  $tanggalLibur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TanggalLibur $tanggalLibur)
    {
        //PROSES SIMPAN EDIT TANGGAL LIBUR
        // return redirect(route('tanggal-libur.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TanggalLibur  $tanggalLibur
     * @return \Illuminate\Http\Response
     */
    public function destroy(TanggalLibur $tanggalLibur)
    {
        //PROSES HAPUS TANGGAL LIBUR
        // return redirect(route('tanggal-libur.index'));
    }
}
