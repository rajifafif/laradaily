<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UnitKerja;
use App\Models\Divisi;
use App\Models\User;
use App\Models\Timesheet;
use App\Models\Activity;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $divisis USED IN DROPDOWN
        $user = auth()->user();
        $divisis = Divisi::with(['unit_kerjas' => function($q) use($user){
            if ($user->hasRole(['KaBiro', 'Manager'])) {
                return $q->where('idUnitKerja', $user->unit_kerja_id);
            }
        }]);
        
        if ($user->hasRole(['GM', 'KaBiro', 'Manager'])) {
            $divisis = $divisis->where('idDivisi', $user->divisi_id);
        }

        $divisis = $divisis->get();
        return view('report.show', compact('divisis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = $request->validate([
            'periode_awal' => 'required',
            'periode_akhir' => 'required',
            'unit_kerja_id' => 'required',
            'output' => 'required',
        ]);


        //BLOCK IF KABIRO TRYING TO REQUEST ANOTHER BIRO
        $user = auth()->user();
        if ($user->hasRole('KaBiro'|'GM') && $user->unit_kerja_id != $request->unit_kerja_id) {
            abort(404);
        }


        $data['periode_awal'] = Carbon::parse($data['periode_awal'])->format('Y-m-d');
        $data['periode_akhir'] = Carbon::parse($data['periode_akhir'])->format('Y-m-d');

        $reports = User::with(['timesheets' => function($timesheet) use($data) {
                               $timesheet->whereHas('activity')->whereBetween('date', [$data['periode_awal'], $data['periode_akhir']])->orderBy('date', 'ASC');
                            }, 'timesheets.activity'])
                    ->where('unit_kerja_id', $data['unit_kerja_id'])
                    ->whereHas('roles', function($q) use($user) {
                        $q->where('name', 'Staff');
                        if ($user->hasRole('KaBiro')) {
                            $q->orWhere('name', 'Manager');
                        }
                    })
                    ->where('active', 1)
                    ->get();
        
        //PROCESS USER TIMESHEET SO CAN BE CALLED USING ARRAY INDEX
        foreach ($reports as $key => $report) {
            $timesheet_in_array = [];
            foreach($report->timesheets as $timesheet) {
                $date = Carbon::parse($timesheet->date);
                $thn = $date->format('Y');
                $bln = $date->translatedFormat('F');
                $tgl = $date->format('d');

                $timesheet_in_array[$thn][$bln][$tgl] = 1 + (isset($timesheet_in_array[$thn][$bln][$tgl]) ? 1 : 0);
            }

            $reports[$key]->timesheet_in_array = $timesheet_in_array;
        }        

        /// $divisis USED IN DROPDOWN
        $divisis = Divisi::with(['unit_kerjas' => function($q) use($user){
            if ($user->hasRole(['KaBiro', 'Manager'])) {
                return $q->where('idUnitKerja', $user->unit_kerja_id);
            }
        }]);
        
        if ($user->hasRole(['GM', 'KaBiro', 'Manager'])) {
            $divisis = $divisis->where('idDivisi', $user->divisi_id);
        }
        $divisis = $divisis->get();

        //CREATE ARRAY FOR GENERATE TABLE
        $periode_awal = Carbon::parse($request->periode_awal);
        $periode_akhir = Carbon::parse($request->periode_akhir);

        $table_date = [];
        for($date=$periode_awal; $date<=$periode_akhir; $date=$date->addDays(1)){
            $thn = $date->format('Y');
            $bln = $date->translatedFormat('F');
            $tgl = $date->format('d');

            $table_date[$thn][$bln][$tgl] = $date->isWeekday();
        }
        //END CREATE ARRAY FOR GENERATE TABLE

        $selected_unit_kerja = UnitKerja::where('idUnitKerja', $request->unit_kerja_id)->first();
        $selected_ka_biro = User::where('unit_kerja_id', $request->unit_kerja_id)
                    // ->whereHas('roles', function($q) use ($user){
                    //     if ($user->hasRole('Manager')) {
                    //         $q->where('name', 'Manager');
                    //     } else {
                    //         $q->where('name', 'KaBiro');
                    //     }
                    // })
                    ->first();

        $data = compact('divisis', 'request', 'reports', 'table_date', 'selected_unit_kerja', 'selected_ka_biro'); 

        //IF TYPE PRINT GOES HERE
        if ($request->output == 'print') {
            $unit_kerja = UnitKerja::find($request->unit_kerja_id);

            // return view('report.print', $data + compact('unit_kerja'));
            $pdf = PDF::loadView('report.print', $data + compact('unit_kerja'));
            return $pdf->setPaper('A4', 'landscape')->stream('DailyActiviytReport.pdf');
        }

        //IF TYPE NOT PRINT GOES HERE
        return view('report.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
