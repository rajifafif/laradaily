<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Timesheet;
use Carbon\Carbon;

class RencanaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $timesheets = Timesheet::where('idEmployee', $user->id)
                    //->thisMonth()
                     ->orderBy('date', 'DESC')
                     ->orderBy('time_from', 'DESC')
//                    ->orderBy('date_submit', 'DESC')
                    ->paginate(10);

        return view('rencana-kegiatan.index', compact('user', 'timesheets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //iki wis ben ngehandle sing user e ngimput rencana nggo deknen dewe
    {
        $user = auth()->user();
        $data = $request->all();
        $data['date'] = Carbon::parse($request->date)->format('Y-m-d');
        $data['time_from'] = Carbon::parse($request->time_from)->format('H:i:s');
        $data['time_to'] = Carbon::parse($request->time_to)->format('H:i:s');
        $data['idEmployee'] = $user->id;
        $data['submitBy'] = $user->nickname;

        //Validator Jika Input Lebih dari Jam 7
        $limit = now()->format('Y-m-d').' 07:00:00';
        $limit = Carbon::parse($limit);
        $tempDate = Carbon::parse($data['date']);//Harus masuk temp agar tidak ganti data input
        $dayDiff = now()->startOfDay() //Hari Ini
                        ->diffInDays($tempDate->startOfDay());
        if ($limit < now() && $dayDiff <= 0) {
            return redirect()->back()->withInput()->withErrors(['date' => 'Tidak Dapat Input Rencana Dihari yang sama Kurang dari jam '.$limit->format('H:i')]);
        }
        //End Validator

        $time_sheet = Timesheet::create($data);

        if ($time_sheet) {
            return redirect(route('rencana-kegiatan.index'));
        }

        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $timesheet = Timesheet::where('idEmployee', auth()->user()->id)->where('idTimeSheet', $id)->firstOrFail();

        return view('rencana-kegiatan.edit', compact('timesheet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
