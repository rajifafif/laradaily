<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Activity extends Model
{
    protected $table = 'activity';
    protected $primaryKey = 'idActivity';
    public $timestamps = false;

    protected $fillable = [
    	'idTimeSheet',
    	'realisasiEmployee',
    	'time_from_act',
    	'time_to_act',
    	'commentAtasan',
    	'nicknameBoss',
    	'commentBoss',
    ];

    public function getTimeFromActAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getTimeToActAttribute($value)
    {
        return Carbon::parse($value);
    }
    
    public function timesheet()
    {
        return $this->belongsTo(Timesheet::class, 'idTimeSheet');
    }
    
    public function employee()
    {
        return $this->hasOneThrough(User::class, Timesheet::class);
    }
}
