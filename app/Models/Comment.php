<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
	use SoftDeletes;
    
    protected $fillable = [
    	'idTimeSheet',
    	'user_id',
    	'comment'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function timesheet()
    {
    	return $this->belongsTo(Timesheet::class, 'user_id');
    }
}
