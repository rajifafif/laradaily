<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Timesheet extends Model
{
    protected $table = 'timeSheet';
    protected $primaryKey = 'idTimeSheet';
    const CREATED_AT = 'date_submit';
    const UPDATED_AT = null;

    protected $fillable = [
    	'idEmployee',
    	'date',
    	'time_from',
    	'time_to',
    	'submitBy',
    	'date_submit',
    	'rencanaEmployee',
        'nilai',
    ];

    protected $dates = [
        'date',
    ];

    public function getTimeFromAttribute($value)
    {
        return Carbon::parse($value);
    }
    public function getTimeToAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function scopeThisMonth($query)
    {
        return $query->whereRaw('YEAR(date) = YEAR(CURRENT_DATE())')
                    ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE())');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'idEmployee');
    }
    
    public function employee()
    {
        return $this->user();
    }
    
    public function activity()
    {
        return $this->hasOne(Activity::class, 'idTimeSheet');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'idTimeSheet');
    }

    // public function kabid_comments()
    // {
    //     return $this->comments()->whereHas('user', function($q){
    //         $q->hasRole('KaBid');
    //     });
    // }

    // public function direksi_comments()
    // {
    //     return $this->comments()->whereHas('user', function($q){
    //         $q->hasRole('Direksi');
    //     });
    // }
}
