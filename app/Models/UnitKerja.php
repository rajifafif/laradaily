<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $table = 'unitKerja';
    protected $primaryKey = 'idUnitKerja';
    public $timestamps = false;

    protected $fillable = [
    	'unitKerja',
    	'statusAktif',
    ];

    public function users()
    {
    	return $this->hasMany(User::class, 'unit_kerja_id');
    }

    public function employees()
    {
    	return $this->users();
    }

    public function divisi()
    {
        return $this->belongsTo(Divisi::class, 'idDivisi');
    }
}
