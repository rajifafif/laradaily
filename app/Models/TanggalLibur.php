<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TanggalLibur extends Model
{
    protected $table = 'tbl_libur';
    protected $primaryKey = 'idTgl_libur';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idTgl_libur','tgl_libur', 'keterangan_libur'
    ];

}
