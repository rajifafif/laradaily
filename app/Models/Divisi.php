<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    protected $table = 'divisi';
    protected $primaryKey = 'idDivisi';
    public $timestamps = false;

    protected $fillable = [
    	'txtDivisi',
    ];

    public function unit_kerjas()
    {
    	return $this->hasMany(UnitKerja::class, 'idDivisi');
    }
}
