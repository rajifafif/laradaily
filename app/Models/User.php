<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'nickname', 'username', 'email', 'password', 'unit_kerja_id', 'divisi_id', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function timesheets()
    {
        return $this->hasMany(Timesheet::class, 'idEmployee', 'id');
    }

    public function divisi()
    {
        return $this->hasOne(Divisi::class, 'idDivisi', 'divisi_id');
    }
    public function unitKerja()
    {
        return $this->hasOne(UnitKerja::class, 'idUnitKerja', 'unit_kerja_id');
    }

    public function scopeHasDivisi($query, $divisi_id)
    {
        return $query->where('divisi_id', $divisi_id);
    }

    public function scopeHasUnitKerja($query, $unit_kerja_id)
    {
        return $query->where('unit_kerja_id', $unit_kerja_id);
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
