<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_names = [
            'Admin',
            'Direksi',
            'KaBiro',
            'Manager',
            'Staff'
        ];

        foreach ($role_names as $role_name) {
            Role::firstOrCreate(['name' => $role_name]);
        }
    }
}
