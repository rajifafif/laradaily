<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name', 'Admin')->first();
        $direksi = Role::where('name', 'Direksi')->first();
        $kaBiro = Role::where('name', 'KaBiro')->first();
        $staff = Role::where('name', 'Staff')->first();
        $manager = Role::where('name', 'Manager')->first();

        $modules = [
        	'rencana-kegiatan',
        	'realisasi-kegiatan',
        	'job-order-staff',
        	'report',
        	'chart',
        ];

        foreach ($modules as $module) {
        	 /**Create Permission to INDEX */
            $index = Permission::firstOrCreate(['name' => $module.'-index'], ['name' => $module.'-index']);

            /**Create Permission to CREATE */
            $create = Permission::firstOrCreate(['name' => $module.'-create'], ['name' => $module.'-create']);
            
            /**Create Permission to EDIT */
            $edit = Permission::firstOrCreate(['name' => $module.'-edit'], ['name' => $module.'-edit']);
            
            /**Create Permission to VIEW */
            $show = Permission::firstOrCreate(['name' => $module.'-show'], ['name' => $module.'-show']);
            
            /**Create Permission to DELETE */
            $delete = Permission::firstOrCreate(['name' => $module.'-delete'], ['name' => $module.'-delete']);

            /**Attach Permission to Admin */
            $admin_modules = $modules;
            if(in_array($module, $admin_modules)) {
                $admin->syncPermissions([$index, $create, $edit, $show, $delete]);   
            }

            /**Attach Permission to Direksi */
            $direksi_modules = $modules;
            if(in_array($module, $direksi_modules)) {
                $direksi->syncPermissions([$index, $create, $edit, $show, $delete]);   
            }

            /**Attach Permission to KaBiro */
            $kaBiro_modules = $modules;
            if(in_array($module, $kaBiro_modules)) {
                $kaBiro->syncPermissions([$index, $create, $edit, $show, $delete]);   
            }

            /**Attach Permission to Staff */
            $staff_modules = $modules;
            unset($staff_modules['job-order-staff']);
            unset($staff_modules['report']);
            if(in_array($module, $staff_modules)) {
                $staff->syncPermissions([$index, $create, $edit, $show, $delete]);   
            }
        }
    }
}
