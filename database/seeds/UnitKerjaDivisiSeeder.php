<?php

use Illuminate\Database\Seeder;
use App\Models\UnitKerja;

class UnitKerjaDivisiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //**unitkerja description
        UnitKerja::whereIn('idUnitKerja', [1,2,3,4,5,6])->update(['idDivisi' => 1]);
        UnitKerja::whereIn('idUnitKerja', [15,16,17,18,14])->update(['idDivisi' => 2]);
        UnitKerja::whereIn('idUnitKerja', [11,12,13])->update(['idDivisi' => 3]);
        UnitKerja::whereIn('idUnitKerja', [7,8,9,10])->update(['idDivisi' => 4]);
        UnitKerja::whereIn('idUnitKerja', [22])->update(['idDivisi' => 5]);
        UnitKerja::whereIn('idUnitKerja', [19,20,21])->update(['idDivisi' => 6]);
        
    }
}
