<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class MissingAccountToUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = DB::table('account')->select('account.*')->leftJoin('users', 'account.username', 'users.username')->whereNull('users.username')->get();

        foreach ($accounts as $account) {
        	$user = User::create([
                // 'id' => $account->idEmployee,
        		'name' => $account->namaLengkap,
        		'email' => strtolower($account->username).'@default.com',
        		'nickname' => $account->nickname,
        		'username' => $account->username,
        		'password' => Hash::make($account->password),
        		'active' => $account->statusAktif,
        		'unit_kerja_id' => $account->idUnitKerja,
        		'divisi_id' => $account->idDivisi,
        	]);

        	$role_name = null;
        	switch($account->idRole) {
        		case 1:
        			$role_name = 'Admin';
        			break;
        		case 2:
        			$role_name = 'Direksi';
        			break;
        		case 3:
        			$role_name = 'KaBiro';
        			break;
        		case 4:
        			$role_name = 'Staff';
        			break;
        	}
        	if ($role_name) {
        		$user->assignRole($role_name);
        	}
        }
    }
}
