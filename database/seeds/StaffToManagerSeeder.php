<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class StaffToManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Dalvin choose to change the ID of staff role to 5 and use the id 4 as manager
     *
     * @return void
     */
    public function run()
    {
        $staffRole = Role::where('name', 'Staff')->first();
        if ($staffRole->id == 4) {
            $staffRole->id = 5;
            $changeId = $staffRole->save();

            //Manually change User Roles, because thereis possibility of no constrain
            if ($changeId) {
                $usersWithOldStaffId = User::whereHas('roles', function($role){$role->where('name', 'Staff');})->get();

                foreach ($usersWithOldStaffId as $user) {
                    $user->syncRoles($staffRole);
                }
            }

            //create Manager via DB because Role() can't use custom id;
            DB::insert('insert into roles (id, name, guard_name, created_at, updated_at) values (?, ?, ?, ?, ?)', [4, 'Manager', 'web', now(), now()]);
        }
    }
}
