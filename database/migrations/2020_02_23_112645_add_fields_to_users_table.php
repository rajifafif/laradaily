<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nickname')->after('name')->nullable();
            $table->string('username')->after('nickname')->unique()->nullable();
            $table->integer('unit_kerja_id')->after('email')->nullable();
            $table->integer('divisi_id')->after('unit_kerja_id')->nullable();
            $table->boolean('active')->after('divisi_id')->default(0);

            $table->index(['unit_kerja_id', 'divisi_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nickname');
            $table->dropColumn('username');
            $table->dropColumn('unit_kerja_id');
            $table->dropColumn('divisi_id');
            $table->dropColumn('active');
        });
    }
}
